﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace TestCalculatorXamarin.POM
{
    public class MainScreen
    {
        public static IApp App => AppInitializer.App;

        public static void Repl()
        {
            App.Repl();
        }

        public Query textField = x => x.Id("input");
        public Query DELButton = x => x.Text("DEL");
        public Query zeroButton = x => x.Text("0");
        public Query oneButton = x => x.Text("1");
        public Query twoButton = x => x.Text("2");
        public Query threeButton = x => x.Text("3");
        public Query fourButton = x => x.Text("4");
        public Query fiveButton = x => x.Text("5");
        public Query sixButton = x => x.Text("6");
        public Query sevenButton = x => x.Text("7");
        public Query eightButton = x => x.Text("8");
        public Query nineButton = x => x.Text("9");
        public Query dotButton = x => x.Text(".");
        public Query plusButton = x => x.Text("+");
        public Query minusButton = x => x.Text("-");
        public Query multiplyButton = x => x.Text("*");
        public Query divideButton = x => x.Text("÷");
        public Query equalButton = x => x.Text("=");

        public MainScreen TapOnDELButton()
        {
            App.Tap(DELButton);
            return this;
        }

        public MainScreen TapOnZeroButton()
        {
            App.Tap(zeroButton);
            return this;
        }

        public MainScreen TapOnOneButton()
        {
            App.Tap(oneButton);
            return this;
        }

        public MainScreen TapOnTwoButton()
        {
            App.Tap(twoButton);
            return this;
        }

        public MainScreen TapOnThreeButton()
        {
            App.Tap(threeButton);
            return this;
        }

        public MainScreen TapOnFourButton()
        {
            App.Tap(fourButton);
            return this;
        }

        public MainScreen TapOnFiveButton()
        {
            App.Tap(fiveButton);
            return this;
        }
        
        public MainScreen TapOnsixButton()
        {
            App.Tap(sixButton);
            return this;
        }

        public MainScreen TapOnSevenButton()
        {
            App.Tap(sevenButton);
            return this;
        }

        public MainScreen TapOnEightButton()
        {
            App.Tap(eightButton);
            return this;
        }

        public MainScreen TapOnNineButton()
        {
            App.Tap(nineButton);
            return this;
        }

        public MainScreen TapOnDotButton()
        {
            App.Tap(dotButton);
            return this;
        }

        public MainScreen TapOnPlusButton()
        {
            App.Tap(plusButton);
            return this;
        }
        public MainScreen TapOnMinusButton()
        {
            App.Tap(minusButton);
            return this;
        }

        public MainScreen TapOnMultiplyButton()
        {
            App.Tap(multiplyButton);
            return this;
        }
        public MainScreen TapOnDivideButton()
        {
            App.Tap(divideButton);
            return this;
        }

        public MainScreen TapOnEqualButton()
        {
            App.Tap(equalButton);
            return this;
        }


        public string GetTextFromField()
        {
            return App.Query(textField)[0].Text.Trim();
        }

        public void GetNumber(int number)
        {

            switch (number)
            {
                case 1:
                    TapOnOneButton();
                    break;
                case 2:
                    TapOnTwoButton();
                    break;
                case 3:
                    TapOnThreeButton();
                    break;
                case 4:
                    TapOnFourButton();
                    break;
                case 5:
                    TapOnFiveButton();
                    break;
                case 6:
                    TapOnsixButton();
                    break;
                case 7:
                    TapOnSevenButton();
                    break;
                case 8:
                    TapOnEightButton();
                    break;
                case 9:
                    TapOnNineButton();
                    break;
                case 0:
                    TapOnZeroButton();
                    break;
                default:
                    break;
            }

        }
    }
}
