﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using TestCalculatorXamarin.POM;

namespace TestCalculatorXamarin.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        MainScreen mainScreen = new MainScreen();


        [BeforeTestRun]
        public static void BeforeEachTest()
        {
            AppInitializer.StartApp();
        }
        
        [When(@"Tap on first number is (.*)")]
        public void WhenTapOnFirstNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"Tap on button plus")]
        public void WhenTapOnButtonPlus()
        {
            mainScreen.TapOnPlusButton();
        }

        [When(@"Tap on second number is (.*)")]
        public void WhenTapOnSecondNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"Tap on button equal")]
        public void WhenTapOnButtonEqual()
        {
            mainScreen.TapOnEqualButton();
        }

        [When(@"Tap on button minus")]
        public void WhenTapOnButtonMinus()
        {
            mainScreen.TapOnMinusButton();
        }

        [When(@"Tap on button divide")]
        public void WhenTapOnButtonDivide()
        {
            mainScreen.TapOnDivideButton();
        }

        [When(@"Tap on button multiply")]
        public void WhenTapOnButtonMultiply()
        {
            mainScreen.TapOnMultiplyButton();
        }

        [When(@"Tap on (.*) number")]
        public void WhenTapOnNumber(double p0)
        {
            double resultDouble = Math.Truncate(p0);
            int resultiInt = Convert.ToInt32(resultDouble);
            mainScreen.GetNumber(resultiInt);
            mainScreen.TapOnDotButton();
            p0 = (p0 - resultDouble)*10;
            int resultint = Convert.ToInt32(p0);
            mainScreen.GetNumber(resultint);
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            string actual = mainScreen.GetTextFromField();
            int result = Convert.ToInt32(actual);

            Assert.AreEqual(p0, result);
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(double p0)
        {
            string actual = mainScreen.GetTextFromField();
            double result = Convert.ToDouble(actual);

            Assert.AreEqual(p0, result);
        }

        [AfterScenario]
        public void FieldCleanup()
        {
            mainScreen.TapOnDELButton();
        }
    }
}
