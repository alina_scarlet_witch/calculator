﻿
using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;

namespace CalculatorTestsAppium.POM
{
    public class MainScreen
    {
        public AndroidDriver<AndroidElement> _driver;

        public MainScreen(AndroidDriver<AndroidElement> driver)
        {
            this._driver = driver;
        }

        public By oneButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[11]");
        public By twoButton = By.XPath("/ hierarchy / android.widget.FrameLayout / android.widget.LinearLayout / android.widget.FrameLayout / android.widget.LinearLayout / android.view.ViewGroup / android.widget.Button[12]");
        public By threeButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[13]");
        public By fourButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[7]");
        public By fiveButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[8]");
        public By sixButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[9]");
        public By sevenButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[3]");
        public By eightButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[4]");
        public By nineButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[5]");
        public By zeroButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[16]");
        public By plusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]");
        public By minusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[10]");
        public By multiplicationButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[6]");
        public By divisionButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[2]");
        public By dotButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[15]");
        public By DELButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[1]");
        public By equalButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]");
        public By textField = By.XPath("/ hierarchy / android.widget.FrameLayout / android.widget.LinearLayout / android.widget.FrameLayout / android.widget.LinearLayout / android.widget.HorizontalScrollView / android.widget.TextView");

        public MainScreen TapOnOneButton()
        {
            _driver.FindElement(oneButton).Click();
            return this;
        }

        public MainScreen TapOnTwoButton()
        {
            _driver.FindElement(twoButton).Click();
            return this;
        }

        public MainScreen TapOnThreeButton()
        {
            _driver.FindElement(threeButton).Click();
            return this;
        }

        public MainScreen TapOnFourButton()
        {
            _driver.FindElement(fourButton).Click();
            return this;
        }

        public MainScreen TapOnFiveButton()
        {
            _driver.FindElement(fiveButton).Click();
            return this;
        }

        public MainScreen TapOnSixButton()
        {
            _driver.FindElement(sixButton).Click();
            return this;
        }

        public MainScreen TapOnSevenButton()
        {
            _driver.FindElement(sevenButton).Click();
            return this;
        }

        public MainScreen TapOnEightButton()
        {
            _driver.FindElement(eightButton).Click();
            return this;
        }

        public MainScreen TapOnNineButton()
        {
            _driver.FindElement(nineButton).Click();
            return this;
        }

        public MainScreen TapOnZeroButton()
        {
            _driver.FindElement(zeroButton).Click();
            return this;
        }

        public MainScreen TapOnPlusButton()
        {
            _driver.FindElement(plusButton).Click();
            return this;
        }

        public MainScreen TapOnMinusButton()
        {
            _driver.FindElement(minusButton).Click();
            return this;
        }

        public MainScreen TapOnMultiplicationButton()
        {
            _driver.FindElement(multiplicationButton).Click();
            return this;
        }

        public MainScreen TapOnDivisionButton()
        {
            _driver.FindElement(divisionButton).Click();
            return this;
        }

        public MainScreen TapOnDotButton()
        {
            _driver.FindElement(dotButton).Click();
            return this;
        }

        public MainScreen TapOnDELButton()
        {
            _driver.FindElement(DELButton).Click();
            return this;
        }
        public MainScreen TapOnEqualButton()
        {
            _driver.FindElement(equalButton).Click();
            return this;
        }


        public AndroidElement FindElementTextField()
        {
            return _driver.FindElement(textField);

        }

        public string Gettext()
        {

            return FindElementTextField().Text;
        }

        public void GetNumber(int number)
        {

            switch (number)
            {
                case 1:
                    TapOnOneButton();
                    break;
                case 2:
                    TapOnTwoButton();
                    break;
                case 3:
                    TapOnThreeButton();
                    break;
                case 4:
                    TapOnFourButton();
                    break;
                case 5:
                    TapOnFiveButton();
                    break;
                case 6:
                    TapOnSixButton();
                    break;
                case 7:
                    TapOnSevenButton();
                    break;
                case 8:
                    TapOnEightButton();
                    break;
                case 9:
                    TapOnNineButton();
                    break;
                case 0:
                    TapOnZeroButton();
                    break;
                default:
                    break;

            }

        }

    }
}

