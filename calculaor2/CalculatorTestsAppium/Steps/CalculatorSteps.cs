﻿using CalculatorTestsAppium.POM;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;

namespace CalculatorTestsAppium.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        public static AndroidDriver<AndroidElement> driver;
        public MainScreen mainScreen;


        [BeforeScenario]
        public void CreateDriver()
        {
            AppiumOptions caps = new AppiumOptions();

            caps.AddAdditionalCapability("app", @"C:\\Users\\123\\Documents\\folder\\calculaor2\\calculaor\\bin\\Release\\com.companyname.calculaor.apk");
            caps.AddAdditionalCapability("deviceName", "Pixel 2");
            caps.AddAdditionalCapability("automationName", "UIAutomator2");
            caps.AddAdditionalCapability("platformVersion", "9.0");
            caps.AddAdditionalCapability("platformName", "Android");
            caps.AddAdditionalCapability("build", "CSharp Android_Sim");
            caps.AddAdditionalCapability("name", "second_test");

            driver = new AndroidDriver<AndroidElement>(
                    new Uri("http://localhost:4723/wd/hub"), caps);
            mainScreen = new MainScreen(driver);
        }

        [When(@"Tap on first number is (.*)")]
        public void WhenTapOnFirstNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"Tap on button plus")]
        public void WhenTapOnButtonPlus()
        {
            mainScreen.TapOnPlusButton();
        }

        [When(@"Tap on second number is (.*)")]
        public void WhenTapOnSecondNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"Tap on button equal")]
        public void WhenTapOnButtonEqual()
        {
            mainScreen.TapOnEqualButton();
        }

        [When(@"Tap on button minus")]
        public void WhenTapOnButtonMinus()
        {
            mainScreen.TapOnMinusButton();
        }

        [When(@"Tap on button divide")]
        public void WhenTapOnButtonDivide()
        {
            mainScreen.TapOnDivisionButton();
        }

        [When(@"Tap on button multiply")]
        public void WhenTapOnButtonMultiply()
        {
            mainScreen.TapOnMultiplicationButton();
        }

        [When(@"Tap on (.*) number")]
        public void WhenTapOnNumber(double p0)
        {
            double resultDouble = Math.Truncate(p0);
            int resultiInt = Convert.ToInt32(resultDouble);
            mainScreen.GetNumber(resultiInt);
            mainScreen.TapOnDotButton();
            p0 = (p0 - resultDouble) * 10;
            int resultint = Convert.ToInt32(p0);
            mainScreen.GetNumber(resultint);
        }
        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            string actual = mainScreen.Gettext();
            int result = Convert.ToInt32(actual);

            Assert.AreEqual(p0, result);
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(double p0)
        {
            string actual = mainScreen.Gettext();
            double result = Convert.ToDouble(actual);

            Assert.AreEqual(p0, result);
        }

        [AfterScenario]
        public void FieldCleanup()
        {
            mainScreen.TapOnDELButton();
        }

        [AfterTestRun]

        public static void QuitDriver()
        {
            driver.Quit();
        }
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
