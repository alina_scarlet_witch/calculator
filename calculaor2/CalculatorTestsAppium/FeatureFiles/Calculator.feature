﻿Feature: Calculator
	As a user
	I want to be able to add two numbers
	In order to solve mathematical equations


Scenario Outline: Add  two  numbers
	When Tap on first number is <firstNum>
	And Tap on button plus 
	And Tap on second number is <secondNum>
	And Tap on button equal
	Then the result should be <result>

	Examples:
		| firstNum | secondNum | result |
		| 5        | 4         | 9      |
		| 6        | 2         | 8      |

Scenario Outline: Difference of two numbers
	When Tap on first number is <firstNum>
	And Tap on button minus 
	And Tap on second number is <secondNum>
	And Tap on button equal 
	Then the result should be <result>

	Examples:
		| firstNum | secondNum | result |
		| 5        | 4         | 1      |
		| 6        | 2         | 4      |

Scenario Outline: Divide two numbers
	When Tap on first number is <firstNum>
	And Tap on button divide 
	And Tap on second number is <secondNum>
	And Tap on button equal 
	Then the result should be <result>

	Examples:
		| firstNum | secondNum | result |
		| 9        | 3         | 3      |
		| 6        | 2         | 3      |

Scenario Outline: Multiplication of two  numbers
	When Tap on first number is <firstNum>
	And Tap on button multiply 
	And Tap on second number is <secondNum>
	And Tap on button equal 
	Then the result should be <result>

	Examples:
		| firstNum | secondNum | result |
		| 4        | 2         | 8      |
		| 3        | 3         | 9      |

Scenario: Adding fractional numbers
	When Tap on 1.9 number
	And Tap on button plus
	And Tap on 3.7 number
	And Tap on button equal 
	And Tap on button plus
	And Tap on 4.2 number
	And Tap on button equal 
	Then the result should be 9.8

