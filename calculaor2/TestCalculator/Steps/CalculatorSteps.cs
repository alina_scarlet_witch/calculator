﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;
using TestCalculatorBrowserStack.POM;

namespace TestCalculatorBrowserStack.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        static AndroidDriver<AndroidElement> driver;
 
        [BeforeTestRun]

        public static void CreateDriver()
        {
            AppiumOptions caps = new AppiumOptions();

            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "bsuser_sEM61O");
            caps.AddAdditionalCapability("browserstack.key", "C7B7xopRoW46UBejqdtz");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://ee23bdc723e8e0a9e24ebeb173cc29ac467816e4");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 3");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "first_test");


            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above
            driver = new AndroidDriver<AndroidElement>(
                     new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);
        }

        public MainScreen mainScreen = new MainScreen(driver);

        [When(@"Tap on first number is (.*)")]
        public void WhenTapOnFirstNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"Tap on button plus")]
        public void WhenTapOnButtonPlus()
        {
            mainScreen.TapOnPlusButton();
        }

        [When(@"Tap on second number is (.*)")]
        public void WhenTapOnSecondNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }

        [When(@"Tap on button equal")]
        public void WhenTapOnButtonEqual()
        {
            mainScreen.TapOnEqualButton();
        }

        [When(@"Tap on button minus")]
        public void WhenTapOnButtonMinus()
        {
            mainScreen.TapOnMinusButton();
        }

        [When(@"Tap on button divide")]
        public void WhenTapOnButtonDivide()
        {
            mainScreen.TapOnDivisionButton();
        }

        [When(@"Tap on button multiply")]
        public void WhenTapOnButtonMultiply()
        {
            mainScreen.TapOnMultiplicationButton();
        }

        [When(@"Tap on (.*) number")]
        public void WhenTapOnNumber(double p0)
        {
            double resultDouble = Math.Truncate(p0);
            int resultiInt = Convert.ToInt32(resultDouble);
            mainScreen.GetNumber(resultiInt);
            mainScreen.TapOnDotButton();
            p0 = (p0 - resultDouble) * 10;
            int resultint = Convert.ToInt32(p0);
            mainScreen.GetNumber(resultint);
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            string actual = mainScreen.Gettext();
            int result = Convert.ToInt32(actual);

            Assert.AreEqual(p0, result);

        }
        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(double p0)
        {
            string actual = mainScreen.Gettext();
            double result = Convert.ToDouble(actual);

            Assert.AreEqual(p0, result);
        }

        [AfterScenario]
        public void FieldCleanup()
        {
            mainScreen.TapOnDELButton();
        }

        [AfterTestRun]

        public static void QuitDriver()
        {
            driver.Quit();
        }

    }
}

